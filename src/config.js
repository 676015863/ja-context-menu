export default {
  cssId: 'ja-contextmenu-css',
  panelClassName: 'ja-panel',
  wrapperClassName: 'ja-contextmenu',
  defaultMenuWidth: 200,
  // childMenuWidth: 150, // 自动继承父菜单宽度
  menuItemHeight: 24,
  menuItemDivideLineMargin: 5, // type == 'divide'
  baseZIndex: 1000, // 基准z-index
};
