declare namespace _default {
    const cssId: string;
    const panelClassName: string;
    const wrapperClassName: string;
    const defaultMenuWidth: number;
    const menuItemHeight: number;
    const menuItemDivideLineMargin: number;
    const baseZIndex: number;
}
export default _default;
