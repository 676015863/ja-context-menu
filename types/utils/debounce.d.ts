export default function debounce(func: Function, delay: number, immediate: boolean): Function;
