export default interface ContextMenuOption {
    hideMenuWhenScroll?: boolean;
    fixMenuWhenScroll?: boolean;
}
