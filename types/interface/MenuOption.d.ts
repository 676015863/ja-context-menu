import MenuItemOption from './MenuItemOption';
export default interface MenuOption {
    width?: number;
    items?: MenuItemOption[];
}
